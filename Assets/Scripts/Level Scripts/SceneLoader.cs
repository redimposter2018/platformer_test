using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
    int sceneCount;
    public int activeScene;

    void Start()
    {
        activeScene = SceneManager.GetActiveScene().buildIndex;
        sceneCount = SceneManager.sceneCountInBuildSettings - 1;
    }
    public void LoadLevel()
    {
        if (activeScene == sceneCount)
            SceneManager.LoadScene(0);
        else
            SceneManager.LoadScene(activeScene + 1);
    }
}
