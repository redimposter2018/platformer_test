using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleScript : Entity
{
    private void Awake()
    {
        lives = 2;
        god = true;
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject == Hero.Instance.gameObject)
        {
            Hero.Instance.GetDamage();
        }
    }
}
