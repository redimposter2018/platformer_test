using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WormScript : Entity
{
    private void Awake()
    {
        lives = 3;
        god = false;
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject == Hero.Instance.gameObject)
        {
            Hero.Instance.GetDamage();
        }
    }
}
