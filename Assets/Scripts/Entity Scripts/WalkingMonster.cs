using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WalkingMonster : Entity
{
    Vector3 dir;
    Vector3 dirSphere;
    SpriteRenderer sprite;

    [SerializeField] bool curentIsDamage;
    private void Awake()
    {
        
        sprite = transform.GetComponentInChildren<SpriteRenderer>();
    }

    private void Start()
    {
        dir = transform.right;
        dirSphere = transform.right;
        lives = 5;
        god = false;
    }

    private void Update()
    {
        curentIsDamage = isDamage;
        Move();
    }

    private void Move()
    {
        Collider2D[] colliders = Physics2D.OverlapCircleAll(transform.position + transform.up * 0.2f + transform.right * dirSphere.x * 0.7f, 0.1f);

        if(colliders.Length > 1)
        {
            if(transform.rotation.y == 0)
                transform.rotation = new Quaternion(0, 180, 0, 0);
            else
                transform.rotation = new Quaternion(0, 0, 0, 0);
            dir *= -1f;
        }
        transform.position = Vector3.MoveTowards(transform.position, transform.position + dir, Time.deltaTime);
    }
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawSphere(transform.position + transform.up * 0.2f + transform.right * dir.x * 0.7f, 0.1f);
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject == Hero.Instance.gameObject)
        {
            Hero.Instance.GetDamage();
        }
    }
}
