using UnityEngine;

public class Weapon : MonoBehaviour
{
    public float axeSpeed = 15f;

    public Rigidbody2D axeRb;

    private void FixedUpdate()
    {
        axeRb.velocity = transform.right * axeSpeed;

    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.TryGetComponent(out Entity entity))
        {
            entity.GetDamage();
        }
        Destroy(gameObject);
    }
}
