using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Entity : MonoBehaviour
{
    public int lives;
    protected bool god;
    protected bool isDamage = false;

    public virtual void GetDamage()
    {
        if (!god)
        {
            if (!isDamage)
            {
                lives--;
                Hurt();
                gameObject.GetComponentInChildren<SpriteRenderer>().color = new Color32(255, 0, 0, 255);
                StartCoroutine(SwipeColor());
                if (lives < 1)
                {
                    Die();
                }
            }
        }
    }
    protected IEnumerator SwipeColor()
    {
        yield return new WaitForSeconds(0.5f);
        Hurt();
        gameObject.GetComponentInChildren<SpriteRenderer>().color = new Color32(255, 255, 255, 255);
    }
    public void Hurt()
    {
        isDamage = !isDamage;
    }
    public virtual void Die()
    {
        Hero.Instance.enemyCount -= 1;
        Destroy(this.gameObject);
        if (Hero.Instance.enemyCount == 0)
        {
            Hero.Instance.sceneLoader.LoadLevel();
        }
    }
}
