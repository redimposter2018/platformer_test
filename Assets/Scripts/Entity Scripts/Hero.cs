using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Hero : Entity
{
    #region Variables
    [SerializeField] private float speed = 5f; // �������� ��������
    [SerializeField] float jumpForce = 15f; // ���� ������
    public Image[] hearts;
    public Sprite aliveHeart;
    public Sprite deadHeart;
    [SerializeField] AudioSource hurtSound;
    WeaponControll weaponControll;
    public GameObject axeWorldParent;
    public static Hero Instance { get; set; }
    public bool isGrounded = false;
    #region Attack
    public bool isAttacking = false;
    public bool isRecharged = true;

    public Transform attackPos;
    public float attackRange;
    //public Vector3 attackVector = new Vector3(5,5,0);
    public LayerMask enemy;
    #endregion

    Rigidbody2D rb;
    Animator anim;
    Vector3 turn;
    public Camera cam;
    public Vector3 point;
    SpriteRenderer Sr;

    public int enemyCount;
    public SceneLoader sceneLoader;
    #endregion



    private void Awake()
    {
        //cam = GetComponent<Camera>();
        point = cam.WorldToViewportPoint(transform.position);
        Sr = GetComponentInChildren<SpriteRenderer>();
        lives = 5;
        turn = transform.right;
        Instance = this; /// ���� ��������
        isRecharged = true;
        weaponControll = GetComponent<WeaponControll>();
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
    }

    private void Start()
    {
        enemyCount = GameObject.FindGameObjectsWithTag("Enemy").Length;
        //print(enemyCount + " ������ �� ������");
        sceneLoader = gameObject.GetComponent<SceneLoader>();
        //print(sceneLoader.name);
    }

    private void FixedUpdate()
    {
        CheckGrounded();
    }

    private void Update()
    {
        if (isGrounded && Input.GetButtonDown("Jump"))
        {
            Jump();
        }
        if (isGrounded && !isAttacking)
        {
            State = States.idle;
        }
        if (Input.GetButton("Horizontal"))
        {
            Walk();
            point = cam.WorldToViewportPoint(transform.position); //���������� ��������� ������� � �������� ������, X � Y ��� ����� ��� ��� ������� � ������ ������� ������

        }
        if (Input.GetButtonDown("Fire1"))
        {
            Attack();
        }
    }
    private void Walk()
    {
        if (isGrounded && !isAttacking)
        {
            State = States.walk;
        }
        if (Input.GetKey("a"))
        {
            turn = -transform.right * Input.GetAxis("Horizontal");
            transform.rotation = new Quaternion(0, 180, 0, 0);
        }
        else if (Input.GetKey("d"))
        {
            turn = transform.right * Input.GetAxis("Horizontal");
            transform.rotation = new Quaternion(0, 0, 0, 0);

        }
        transform.position = Vector3.MoveTowards(transform.position, transform.position + turn, speed * Time.deltaTime);
    }

    private void Jump()
    {
        rb.AddForce(transform.up * jumpForce, ForceMode2D.Impulse);
    }

    //public void CoolDown()
    //{
    //    isRecharged = !isRecharged;
    //    isAttacking = !isAttacking;
    //}

    void CheckGrounded()
    {
        Collider2D[] colliders = Physics2D.OverlapCircleAll(transform.position, 0.45f); // OverlapCircleAll(�����, ������) - ��������� ������ ���� �����������, ���������� � �������� �������
        isGrounded = colliders.Length > 1; // ���� ����� � ���������� ������ ������ ����������, �� �������� true
        if (!isGrounded && !isAttacking)
        {
            State = States.jump;
        }
    }

    public enum States
    {
        idle,
        walk,
        jump,
        attack
    }

    private States State
    {
        get { return (States)anim.GetInteger("State"); } // �������� ������
        set { anim.SetInteger("State", (int)value); } // ������ ���������
    }



    public void DestroyHeart()
    {

        foreach (var h in hearts)
        {
            h.sprite = deadHeart;
        }
        SceneManager.LoadScene(sceneLoader.activeScene);
    }
    void Attack()
    {
        if (isRecharged)
        {
            isRecharged = false;

            isAttacking = true;
            State = States.attack;
            StartCoroutine(AttackCoolDown());
            //StartCoroutine(AttackAccept());
        }
    }

    IEnumerator AttackCoolDown()
    {
        yield return new WaitForSeconds(1f);
        isRecharged = !isRecharged;
        isAttacking = !isAttacking;
    }
    public override void GetDamage()
    {
        base.GetDamage();
        hurtSound.Play();
        if (lives < 1)
        {
            DestroyHeart();
        }
        else
        {
            hearts[lives].sprite = deadHeart;
        }
    }

    //public void GetDamage()
    //{
    //    if (!isDamage)
    //    {
    //        hurtSound.Play();
    //        //Sr.color = new Color32(255, 0, 0, 255);
    //        health -= 1;
    //        Hurt();
    //        StartCoroutine(SwipeColor());
    //        if (health < 1)
    //        {
    //            DestroyHeart();
    //        }
    //    }

    //}


}
