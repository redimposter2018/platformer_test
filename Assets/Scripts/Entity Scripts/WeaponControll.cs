using UnityEngine;

public class WeaponControll : MonoBehaviour
{
    public float fireRange = 10f;

    public Transform axeSpawn;
    public GameObject axePrefab;

    public void Shoot()
    {
        Collider2D[] colliders = Physics2D.OverlapCircleAll(transform.position, fireRange);
        GameObject clone = Instantiate(axePrefab, axeSpawn.transform);
        clone.transform.SetParent(Hero.Instance.axeWorldParent.gameObject.transform);
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(axeSpawn.transform.position, fireRange);
    }
}
