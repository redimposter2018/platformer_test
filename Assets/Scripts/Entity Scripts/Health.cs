using System.Collections;
using UnityEngine;

public class Health : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject == Hero.Instance.gameObject)
        {
            StartCoroutine(HealingPlayer());
        }

        IEnumerator HealingPlayer()
        {
            Hero.Instance.hearts[Hero.Instance.lives].sprite = Hero.Instance.aliveHeart;
            Hero.Instance.lives++;
            Destroy(gameObject);
            yield return new WaitForSeconds(1f);
        }
    }
}
