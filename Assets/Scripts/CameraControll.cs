using UnityEngine;

public class CameraControll : MonoBehaviour
{
    [SerializeField] Transform player; // �����
    Vector3 pos; // ������� ������

    private void Awake()
    {
        if (!player) // ���������� ������ (����������� ��������)
        {
            player = FindObjectOfType<Hero>().transform;
        }
    }

    private void Update()
    {
        pos = player.position;
        pos.z = -10f; // ������������ Z-���������� ��������, ���� ������ �� ��������
        transform.position = Vector3.Lerp(transform.position, pos, Time.deltaTime);
    }
}
